# Demonizer

2D scrolling shooter demo - by Iori Branford

## About

The outcast princess turned succubus must stop cruel Kingdom troops from slaughtering friendly monstergirls in this 90s-arcade-style vertical shooting and man-catching challenge.

- Blow up war machines and break the soldiers' will with hot demonic seduction blasts
- Collect men to nourish you with spiritual energy; transform women to grow your succubus squadron
- Charge up a massive spiritual/demonic explosion to flush out soldiers in hiding

## How to Play

On PC, play with the keyboard, or a controller or mouse with three buttons. On keyboard, the arrow keys are the directions and the Z,X,C keys are Buttons 1,2,3.

On touchscreen, drag with one finger for directions, Fire, and Focus. Tap with two fingers for bomb.

- Push **directions** to **move** your team of succubi in formation.
  - Move into unarmed or defeated **humans** to **capture** them.
    - Capture **men** to earn increasing capture **bonuses**, **bomb energy**, and **Power Grab** abilities.
    - Capture **women** to transform them into succubus **allies**.
    - You have **limited time** to capture humans before they bleed out, fall, or commit suicide. Any human's death also kills all of your captives, and resets the capture bonus to the minimum value.
  - Move into hostile **enemies** to deal **contact damage.**
- Press and hold **Button 1** to **fire** demonic energy bolts at hostile humans and machines in front of you.
- Press and hold **Button 2** to enter **focus** mode.
  - When playing with keyboard or controller, your movement is **slower** for precision dodging.
  - Allies assume a more aggressive formation. When firing they **autoaim** at nearby enemies.
  - When any member of your team has **20** captives, she gains a long-range **Power Grab** ability. Enter focus mode to activate it.
    - Your Power Grab **pulls** humans towards you.
    - Allies Power Grab by **flying** to humans, but will become **vulnerable** to enemy fire.
- Press **Button 3** to detonate a **bomb** in front of you, if you have at least one bomb gauge full.
  - The bomb blast damages enemies (even **through cover**), **blocks** their fire, and **sends** capturable humans towards you.
  - Allies become invulnerable and **charge** at enemies to do contact damage.
  - Capture men to gain **bomb energy**, and hold onto them to slowly gather more bomb energy over time.
- Press **Start** button or **Escape** key to **pause** the game. Press Up and Down to highlight an option and Button 1 to select it, or click or tap on an option. Press Start or Escape again to resume.

## Links

Game site: [itch.io](https://ioribranford.itch.io/demonizer)

Game source: [Github Repository](https://github.com/ioribranford/demonizer)

[![Demonizer](http://button.indiedb.com/popularity/medium/games/54464.png)](http://www.indiedb.com/games/demonizer)

[Become a Patron](https://www.patreon.com/bePatron?u=6000071) to help fund stretch goals.
