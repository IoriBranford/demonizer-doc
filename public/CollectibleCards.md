# Collectible Cards

Collectible cards earned through gameplay, featuring artwork and game hints.

## Card types

- Hint cards
  - Combat tips
  - Hidden mechanics
- Enemy cards
- Friend cards

## How to get

- Perform associated action
- Defeat associated enemy, or die 3 times to enemy
- Rescue associated friend
- Spend blue hearts accumulated over all plays, or excess cards of equivalent value

## Card List

### Hints

| Card                | Value | Description                                                  | First cond.                                    | Subsequent cond.                                             |
| ------------------- | ----- | ------------------------------------------------------------ | ---------------------------------------------- | ------------------------------------------------------------ |
| Quick Shield        | 2     | You are protected from bullets while turning someone. Grab a red heart and charge through those bullets! | Take a bullet while demonizing                 | Take a bullet and defeat an enemy with touch attack while demonizing |
| Touch Attack        | 1     | Get on top of an enemy to deal continuous damage and block them from attacking. | Touch attack an enemy for the first time       | Defeat an enemy with touch attack                            |
| Can't Hide From Me  | 2     | You can touch attack enemies even while they take cover from your bullets. | Touch attack an enemy through cover            | Defeat an enemy taking cover with touch attack               |
| Touch Heal          | 2     | Get on top of a wounded friend to slowly restore her health. | Use touch heal for the first time              |                                                              |
|                     | 2     | After you collect 20 blue hearts, press ${joy_focus} to pull hearts towards you. | Player gets 20 blue hearts for the first time  |                                                              |
|                     | 2     | After a partner collects 20 blue hearts, holding ${joy_focus} sends her out to grab hearts, but she can be hurt by bullets. | Partner gets 20 blue hearts for the first time |                                                              |
| Gotta Catch 'Em ALL | 3     | For the most points and extra lives, don't let blue hearts fall offscreen or the value will reset to 0. | Get 10000 points from a blue heart             | Finish a stage without losing a blue heart                   |
| The Rim Is Dim      | 1     | Don't hug the edges of the screen.                           |                                                |                                                              |

### Enemies

| Card                | Value | Description                                                  |
| ------------------- | ----- | ------------------------------------------------------------ |
| Pikeman             | 1     | Mass assaulter. Can only face and attack forward.            |
| Archer              | 1     | Cautious marksman. Fast arrow, but slows down to fire.       |
| Swordsman           | 2     | Fast skirmisher. Slow start, short range attack.             |
| Catapult            | 2     | Light artillery with spread fire. Fixed direction.           |
| Angelina - Ranger   | 6     | Dashes between cover firing both arrow types. Takes time in cover to charge fire arrows. |
| Militiaman          | 1     | Flanking assaulter. Can face and attack sideways but not backward. |
| Defender            | 2     | Trident and heavy shield. Slow to move and fire.             |
| Militia Captain     | 5     | Commands the village militia.                                |
| Lord                | 7     | Village leader with custom battle armor.                     |
| Fire Archer         | 2     | Shoots falling fire arrow. Slow to fire.                     |
| Priest              | 3     | Controls orbs.                                               |
| Priest's Orb        | 3     | Grabs blue hearts to power up its attack.                    |
| High Priest         | 7     | Orb and martial arts mastery.                                |
| Zombie              | 1     | Explodes releasing toxins on contact (with obstacles and other enemies too). |
| Butcher             | 2     | Slaughters your friends.                                     |
| Mage                | 3     | Manipulates the environment to fire randomly.                |
| Necromancer         | 5     | Summons zombies from pools and sprays toxins.                |
| Hypnotist           | 4     | Commands your friends to attack you.                         |
| Fire Mage           | 8     | Master of fire magic.                                        |
| Mounted Javelin     | 2     | Throws falling javelin from horseback, if it can reach you.  |
| Mounted Archer      | 2     | Fires arrows at your flank and rear from horseback. Can't fire backwards |
| Mounted Fire Archer | 3     | Fires triple fire arrows but only straight up.               |
| Mounted Swordsman   | 3     | Approaches you for short range sword attack. Can crash into other mounted enemies, knocking them both off. |
| Lancer              | 4     | Very fast lance charge on horseback fires in a 'T'.          |
| Light Sky Armor     | 3     | Armor blessed with flight. Rains slow bullets downward in a fixed direction. |
| Medium Sky Armor    | 4     | Missile stream above or below depending on your position.    |
| Heavy Sky Armor     | 5     |                                                              |
| Snake Sky Armor     | 7     |                                                              |
| Great Sky Armor     | 8     |                                                              |

### Friends

| Card       | Value | Description |
| ---------- | ----- | ----------- |
| Alraune | 2 | |
| Anubis | 2 | |
| Arachne | 2 | |
| Black Harpy | 3 | |
| Blue Slime | 1 | |
| Devil Bug | 1 | |
| Ghost | 1 | |
| Giant Ant | 2 | |
| Goblin | 1 | |
| Harpy | 1 | |
| Holstaurus | 1 | |
| Honeybee | 3 | |
| Hornet | | |
| Inari | 2 | |
| Kappa | 1 | |
| Mermaid | 1 | |
| Mummy | 1 | |
| Orc | 1 | |
| Red Oni | 3 | |
| Siren | 3 | |
| Werebat | 1 | |
| Weresheep | 1 | |
| Werewolf | 2 | |
| Wurm | 4 | |
| Youko | 1 | |