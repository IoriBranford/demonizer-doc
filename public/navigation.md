# Demonizer

[Design]()

* [Game Design](Design.md)
* [Setting](Setting.md)
- - - -
* # Characters
  * [Princess Amelia](Player.md)
  * [Princess Angelina](Sister.md)
  * [King Belius](King.md)
  * [Final Boss](FinalBoss.md)
  * [Other Characters](OtherCharacters.md)
- - - -
* # Stages
  * [Demon Realm](DemonRealm.md)
  * [Village](Village.md)
  * [Chapel](Chapel.md)
  * [Laboratory](Laboratory.md)
  * [Highway](Highway.md)
  * [Capital](Capital.md)
- - - -
* # Extras
  * [Collectible Cards](CollectibleCards.md)

-----------

[Production]()

- [Mapping](Mapping.md)
- Art
- Scripting

[gimmick:theme](slate)

[gimmick:themechooser](Choose theme)
