# Village
## Purpose
The first real challenge of the game.

## Difficulty
Normal.

## Estimated Time
Stage: 1 minute
Mid-Boss: 30 seconds
Boss: 45 seconds

## Background
One of many farming communities generating most of the kingdom's essentials. The lord manages production and defense in exchange for the large shares demanded by the kingdom and himself. The villagers subsist on whatever scraps the lord leaves for them.

## Story
Having fled from the previous stage, Angelina has warned the village of Amy's impending attack and called in reinforcements. Amy arrives just as the village militia and the lord's troops finish preparing their defenses.

## Feel
A fast-moving attack stage. Amy charges in at high speed and the enemy intercepts her.

## Areas

1. Perimeter
    * Wall or fence.
    * Archers stand guard.
1. Farmland
    * Fields growing variety of crops. Most enemies move around the crops to avoid damaging them.
    * Sparse cover. e.g. hand carts for hauling crops.
    * Enemies may spend more time onscreen than in the previous stage as they keep up with Amy.
    * Player could bomb ripe crops to send them flying, then catch them for extra points and bomb energy.
1. Storehouse
    * Guarded by militiamen led by their captain.
    * Limited number of militia reserves waiting inside. Captain calls them out to replace fallen ones.
    * Noncombatants hiding inside; bombing the roof exposes them.
1. Village
    * Dirt road in between opposite rows of small houses.
    * Large household objects for cover, e.g. stacks of wood or grain. Also spaces between houses.
1. Manor
    * The front of the lord's house.
    * For archers, arrow slits in walls and crenellation around the roof.

## New Enemy
|Type           |Qty.|Strafe   |Fire Rate|
|---------------|----|---------|---------|
|Militiaman     |Some|         |Slow     |
|Militia captain|One |
|Lord           |One |
|Lord's Guard   |Few |
