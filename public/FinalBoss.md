# Final Boss Angelina

The king's dying prayer has transformed Angelina into a mad angel. When Amy pursues, Angelina turns to face her and a final battle in the sky ensues.

## Appearance

Four blue-white wings have burst from Angelina's back. Her crown and her enraged eyes glow yellow-white. See also [Angelina](Sister.md)

## Personality

The chief god channels his rage through Angelina. She flies through the sky with beast-like shrieks, raining destruction upon the continent as a last-ditch effort to destroy all monsters.

![](img/orochi.jpg)

## Sprites

### Angel form

![](img/angel-ref.png)

Parts:

- Body - 48x48 frame
- Wing - 64x48 frame - duplicate and flip to make 3 other wings

Body animations:

| Action                       | Facing dirs | Cycle msecs | Cycle dir    | Sugg. frame count |
| ---------------------------- | ----------- | ----------- | ------------ | ----------------- |
| Hover midair with longbow    | S           | 500         | Forward      | 3                 |
| Rapid firing longbow forward | S           | 250         | Forward      | 2                 |
| Rapid firing longbow upward  | S           | 250         | Forward      | 2                 |
| Falling defeated             | Face up     | N/A         | Single frame | 1                 |

Wing animations:

| Action             | Facing dirs | Cycle msecs | Cycle dir | Sugg. frame count |
| ------------------ | ----------- | ----------- | --------- | ----------------- |
| Flap               | S           | 500         | Forward   | 3                 |
| Spread             | S           | 500         | Forward   | 3                 |
| Fold to cover body |             | 500         | Forward   | 4                 |
| Ruined and bloody  | S           | 500         | Forward   | 2                 |

### Dark angel form

Upon defeating Angelina for the last time, Amy catches her and turns her into a dark angel. Now she has two black feathered wings, white hair, red eyes, and her sanity restored. Her costume has changed color from light-blue with blue trim to black with red trim. She flies northward alongside Amy back to the monster lands.

Her sprite proportions are the same as Amy's.

![](img/player-sprite.png)

| Action | Facing dirs | Cycle msecs | Cycle dir | Sugg. frame count |
| ------ | ----------- | ----------- | --------- | ----------------- |
| Fly    | N           | 500         | Pingpong  | 3                 |