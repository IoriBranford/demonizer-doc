# Setting

The medieval fantasy world of Demonizer consists of the human Kingdom and the neighboring Demon Realm.

## Kingdom

The Kingdom is the only human nation on the continent. By conquering a majority of the Demon Realm, King Belius and his predecessors earned the loyalty of the other human states and eventually annexed them.

Thus the Kingdom grew too large and expensive to sustain all the people. Instead it feeds the greed and waste of the nobility while the people spend their lives working or serving in the military for subsistence wages. Church propaganda blames monsters for all the kingdom's problems to keep people loyal.

Now King Belius prepares to seize the last remnant of the Demon Realm, thereby fulfilling his ambition and claiming the entire continent for humankind.

## Demon Realm

"Demon Realm" is the human name for the wild land historically inhabited by monsters. It originally spanned the whole continent until humans arrived and began their conquest, pushing the monsters to the southern edge.

The first monster/woman hybrids started to appear some time during the human invasion.  These "monstergirls" are either monsters who evolved human female characteristics and intelligence, or girls who were born human but spontaneously mutated as they grew up. Both variants soon found each other and formed their own primitive communities in the wild.

Humans despise them as corruptions of God's creation and slay any they find. They believe a curse on the land has transformed their daughters, and completing their conquest of the land will break the curse. In truth, this "curse" is the project of the deity human priests call "Demon Lord". She wills humans and monsters to mix in the hope of bringing them together. She also enriched the remaining monster lands into a natural paradise and taught the monstergirls positive values like empathy and community, in the hope of making peace between humanity and nature and thereby ending all the world's suffering.

## Origins

The world as it is known began when the Earth Goddess discovered a lifeless ball of dust and rock. She created the seas, from which arose clouds that brought rain to the land. The seas and rain mixed with dust to make soil. From the soil she created plants and animals for the sea and the land. At first all was peaceful; animals lived on plants, then passed away into the soil so more plants could grow anew. Among all her creatures two trusted and beloved leaders arose: the great kind bird with feathers in every color of the rainbow, and the wise elder goat with gold-flecked horns and a silver-tufted tail.

Then the Hunter God came looking for sport. He saw the Earth Goddess's peaceful world and it bored him. He wooed her with false love to earn her trust and take her to wife. Then he tied her up, and against her anguished cries he went around with fire and bow, flushing the creatures out of their forests and slaughtering as he pleased. After sating his bloodlust, to the Earth Goddess's horror he came back with three trophies: the rainbow bird's wings, and the elder goat's horns and tail.

The Hunter God had his way with the Earth Goddess well into the night. While he slept the Earth Goddess wept in grief and agony. Then before he awoke a few surviving animals freed her. She put the wings and horns and tail on her body, took the survivors in her arms and flew far away. She began to create new plants and animals armed with all manner of defenses: claws, fangs, spines, stings, thorns, venom. But clouded by righteous anger, she realized too late this would leave the animals with fewer plants to eat. To her dismay, many desperate animals turned their weapons on those who had none, and developed a taste for their meat.

Meanwhile the Hunter God was enraged to awaken and find his prizes gone. He set out to take back the Earth Goddess, ready to kill anything in his path. But the creatures remembered, and were ready for him too. Though he did kill many more of them, they broke his bow and bit him and pierced him and poisoned him, until he fled weakened and humiliated back to the heavens. As he recovered he told false tales of adventure in a cursed realm, of battles with a demon queen and her bloodthirsty monster hordes. Thus he rallied his fellow gods, including his father the Chief God, to war against the Earth Goddess and her world.

...