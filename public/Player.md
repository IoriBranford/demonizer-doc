# Amy

Princess Amelia is King Belius's second and youngest daughter. When her father discovered her illicit romance and put her lover to death, she fled to the demon realm and became a succubus. Now she invades her former Kingdom to liberate the people from her father's tyranny -- by monsterizing them.

## Skills

* Winged flight
* Seduction bullets from eyes
* Shrink men and store them in [left to imagination], draw spiritual energy from them
* Succubize women with kiss
* Blow kiss to launch bomb, shield teammates and order them to charge at enemy

## Appearance

Amy is a beautiful but less-than-ladylike young woman. Her waist-length hair shines an otherworldly blue instead of her sister's golden blond; she cuts it herself since the castle hairdressers were always scared of her (and she never liked any of their styles anyway). Her face too is unlike anyone else's in her family: gently rounded features; wide, warm lavender eyes; small lips touched with pink. Her body is fit and slightly tanned from years of playing and working outdoors with Jacob.

Her succubus transformation endowed her with a pair of elfish ears, two curved horns in her hair, a pair of bat wings spanning as wide as her own height, and a tail tipped with a rounded arrowhead.

As a little girl she repeatedly ruined her dresses playing in the garden. For her sixteenth birthday Jacob gave her a summer dress and sandals which have been her favorite and only outfit ever since.

![](img/amy.png)

### Design references

#### Physical

Succubus (Monster Girl Encyclopedia)

![](img/Succnotebook_cover1.jpg)
![](img/Succubus_Extra0a.jpg)
![](img/Succubus_0.jpg)

#### Wardrobe

Dress

* Color somewhere between orange and magenta

![](img/product-image-277269725.jpg)
![](img/product-image-277269696.jpg)
![](img/product-image-277269729.jpg)

Sandals

![](img/sandals.jpg)
![](img/sandals2.jpg)

## Personality

Amy is cheerful, playful and caring. She loves to discover nice things and share them with others, especially anyone who is suffering. She is oblivious to tradition, decorum and social order, and quickly hardened herself against the derision of her noble peers.

### Interests

Nature, people, socializing, performing arts, music, food

## Assets

All values are estimates and subject to change as needed.

### Sprites

Average size: around 40x32 px in a 64x64 px frame.

Proportions reference:

![](img/player-proportions.png)

| Prio | Action                                   | Cycle secs | After cycle |
| ---- | ---------------------------------------- | ---------- | ----------- |
| A    | Fly upward                               | 0.5        | Loop        |
| A    | Launch bomb upward: blow kiss with both hands | 0.5        | Hold        |
| B    | Fly left: tilt slightly left             | 0.25       | Loop        |
| B    | Fly right: tilt slightly right           | 0.25       | Loop        |
| C    | Victory dance in midair                  | 1          | Hold        |

Priorities: A = Essential; B = Desired; C = Nice to have

After cycle:

* Loop -- Loop back to first frame
* Hold -- Stay on last frame until new action
