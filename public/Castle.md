# Castle

## Purpose

The second final exam, bringing back more past enemies and introducing a couple more nasty tricks before the final battle.

## Areas

1. Perimeter
2. Outer wall
3. Courtyard
   - All types of soldier and war machine
4. Flying up the keep wall
   - Hot oil thrown from windows
   - Flying armors from stage 5
5. Keep roof
   - Prince who was supposed to be married to player
     - Rapid rapier attacks scattering bullets randomly
6. Great hall
   - Archmage hypnotizes up to 4 of your wingmen at a time
     - Wingmen fire slower and orbit archmage protectively
   - Portals send moving objects in unexpected directions
     - Any character or bullet crossing portal line is teleported to corresponding point on destination line
7. Ante room
    - Angelina redux, this time using an autocrossbow with much faster fire rate
       - Assassins back her up occasionally
       - Flees into throne room on defeat
8. Throne room
   - The King is not pleased with Angelina's failure. Angelina turns to Amy and offers to surrender herself, in the hope of saving what remains of the kingdom. But the King comes up behind her and cuts her down for a traitor.
     - This sets a timer for X minutes. To save Angelina, Amy must win the last fight before the time runs out.
   - The King swings his greatsword firing a variety of huge cut-shaped patterns. He can also throw it so it spins around the room firing bullet spirals. He can charge it and throw it straight up into the air and it will sweep the room with a huge laser.

