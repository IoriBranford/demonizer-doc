# Capital

## Purpose

The first "final exam" stage. Extreme challenge with big enemy waves and tougher variations of earlier encounters.

## Zones

Lower ward
- Civilians run for their lives
- Guardsmen stream through the streets constantly

Market

- Stalls provide plenty of cover to archers

Lynching

- Mob leader (midboss) burning monstergirl(s) at the stake; defeat to put out the fire and free monstergirl(s)
- Heavies circle around defending the burning

Bridge

- Archers in boats passing under bridge
- Mages call up golems from the water

Park

- Guards and priests

Church

- Bishop
- Archers and priests on roof

Plaza

- Infinite scrolling
- Flying armor pincer attacks
- Lancers rear attacks

Upper ward

- Archers and swordsmen

Citadel gate

- Angelina runs between turrets
- Lancers charge out of the gate