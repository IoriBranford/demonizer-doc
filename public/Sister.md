# Angelina

Belius's first daughter and heir to the throne. Although she loves and cares for Amy, her duty to the Kingdom forces her to fight off Amy's counterattack.

## Skills

* Multi-arrow fire
* Flaming arrow rain
* Fast move and dodge

## Personality

Angelina is dutiful, well-mannered, virtuous, and compassionate. She genuinely wants the best for the people and takes the responsibilities of her position very seriously. Although frequently frustrated by human and systemic obstacles, she never stops seeking ways to improve quality of life in the Kingdom.

### Interests

Law, civil service, history, fine arts, literature, philosophy

## Appearance

Angelina is a model princess with finely-carved face, deep blue eyes and blond hair cut to neck-length. Her flawless skin and figure hide superhuman physical ability honed by rigorous archery and combat training.

![](img/angelina_concept.png)

The second time she fights Amy she wields a faster-firing autocrossbow.

![autocrossbow](img/autocrossbow.png)

## Sprites

### Archer form

Frame size: 32x32

Proportions reference:

![](img/sister-proportion.png)

Base model:

![human-base](img/human-base.png)

![human-base-ko](img/human-base-ko.png)

Animations:

| Action                                   | Facing Dirs | Cycle Secs | Loop     |
| ---------------------------------------- | ----------- | ---------- | -------- |
| Walk with longbow                        | ESWN        | 0.5        | Pingpong |
| Walk with autocrossbow                   | ESWN        | 0.5        | Pingpong |
| Unarmed lie down                         | Face up     | 2.0        | None     |
| Unarmed defeated, kneeling and breathing | S           | 1.0        | Pingpong |
| Unarmed walk                             | ESWN        | 0.5        | Pingpong |
