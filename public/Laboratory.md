# Laboratory

## Purpose

This stage is focused on rescuing friends and navigating through traps.

## Difficulty

Above normal.

## Estimated Time

Stage: 3 minutes
Boss: 1 minute

## Background

An underground facility carved into the side of a mountain where the kingdom researches and develops weapons against monsters.

## Story

Amy follows the high priest down the stairs to the facility. She leads her team through, rescuing and recruiting monstergirls.

When she meets the head mage, he overflows the lava pit and floods the facility. He and her team fly up the volcano shaft as they battle. When she defeats him, the lava settles back down. Once out of the volcano, she sees the highway to the capital and follows it.

## Feel

Slow and thoughtful, but still some urgency due to the monstergirls in danger.

## New Mechanics

A monstergirl in distress has several seconds to be rescued before she dies. To rescue her, shoot all of the enemies and machinery about to kill her, as well as anything holding her such as a cage or chains. Once rescued, she can come with you and fight alongside you, or she can award points and flee.

A hypnotized monstergirl is forced to fight you against her will. Shoot the controlling enemy to free her. If you instead shoot her, the mind control causes her to self-destruct.

A trap is activated by a trigger on the ground and/or in the air. Shoot out the trigger or the trap before you or an ally moves into it. Examples:

- Mines
- Zombies exploding on contact
- Zombies leaving bullet trails like poison gas, fire, etc.
- Electric barriers
- Falling ceiling

## Areas

1. Entrance

   - Narrow walkway guarded by artillery and easy traps.
2. Processing

   - Monstergirls are evaluated to determine the best use for them, then sent to detention.
3. Detention

   - Monstergirls in cages awaiting torture/slaughter.
     - No time pressure to free them except moving camera.
     - If not freed, no loss of score multiplier.
     - Some join you when freed.
4. Hallway guarded by easy traps.
5. Interrogation

   - Monstergirls tortured for information.
     - Boiling slimes in cauldrons.
     - Electrocuting mermaids in cages.
6. Harvesting
   - Monstergirls about to be harvested for useful parts and substances.
     - Milking and slaughtering holstaurs.
     - Shearing and slaughtering weresheep.
     - Slaughtering machines for holstaurs, weresheep, and orcs.
7. Hallway guarded by slightly hard traps.
8. Laboratory
    - Monstergirls tortured in experiments.
       - Boiling slimegirls in cauldrons.
         - Testing poisons on slimegirls, plantgirls, insectgirls.
          - Intoxicating harpies or beegirls into flying around for target practice.
          - Demonsterization/rehumanization research.

    - Monstergirls mind-controlled to fight player.
 9. Hallway guarded by difficult traps.
10. High-security block

  - More powerful monstergirls imprisoned and operated on here. Harder to rescue.
    - An oni in chains resists being brought in by a team of men led by a mage. She can either be freed or eventually break free herself, but then the mage will quickly put her down unless he is shot first.
    - Necromancer pits his undead horde against monstergirls for sport.
11. Archives

    - Heavily trapped to protect secret information.
12. Waste disposal

    - Lava pit at the bottom of a magically activated volcano, used as an incinerator.