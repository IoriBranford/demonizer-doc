# Other Characters

## Kingdom

Jacob

  - Species: human
  - Age: 8-18
  - Physique: slim
  - Hair: short, dark, unkempt
  - Skin: slight tan
  - Wardrobe: worker's clothes
  - Job: gardener
  - Personality: friendly, playful

## Demon Realm

All monster girls' clothes are made of silk.

"Mom"

  - Species: succubus
  - Age: 36
  - Physique: curvy
  - Hair: long, wavy, light
  - Skin: light
  - Wardrobe: classy swimsuits
  - Job: leader, guardian, sky courier, general help
  - Personality: warm, caring, affectionate

Rania

  - Species: arachne
  - Age: 15
  - Physique: petite
  - Skin: light
  - Hair: short, curly, white
  - Notable features: 4 spider eyes in place of human eyes, spider fingers from nostrils
  - Wardrobe: long summer dresses
  - Job: tailor, weaver
  - Personality: shy, polite, hardworking

Alyra

- Species: catgirl
- Age: 26
- Physique: athletic
- Skin: dark
- Hair: black hime-cut
- Wardrobe: sporty black swimsuits
- Job: fisher, messenger, scavenger
- Personality: aloof, lazy, independent

Elizabeth

  - Nickname: Liza
  - Species: dragon
  - Age: 33
  - Physique: slim
  - Skin: light
  - Hair: long blond ponytail and forelocks; bun and hair clips when working
  - Wardrobe: wrap around bikini top and loincloth, with apron when working
    - No heat protection because of natural heat resistance
  - Job: chemist, tinkerer, smith, baker
  - Personality: mature, thoughtful

Petra

  - Species: gargoyle
  - Age: 22
  - Physique: toned
  - Hair: dark bob cut
  - Skin: gray
  - Wardrobe: loose work shirt over bikini
  - Notable features: gem eyes
  - Accessories: pedestal chained to wrist shackle, used as a stool
  - Job: miner, builder, sculptor, assistant to Liza
  - Personality: cool, quiet

Alba

- Species: dryad
- Age: 31
- Physique: slim
- Hair: green, leafy
- Skin: brown
- Wardrobe: long summer dresses
- Job: fruit farmer, herb doctor, woodworker
- Personality: easygoing hippie

Lauren

  - Species: lamia
  - Age: 26
  - Physique: athletic
  - Hair: white, short, like a boy
  - Skin: pale green
  - Wardrobe: tube top, wraparound skirt
  - Job: dancer, scavenger, general help
  - Personality: playful trickster

Tanny

  - Species: raccoon girl
  - Age: 21
  - Physique: slim
  - Hair: brunette ponytail
  - Skin: slight tan
  - Wardrobe: tank top and short shorts
  - Job: farmhand
  - Personality: cheerful, spunky

Connie

- Species: rabbit girl
- Age: 17
- Physique: chubby
- Hair: curly white
- Skin: pale, freckled
- Wardrobe: halter top and short shorts
- Job: vegetable farmer
- Personality: bubbly

Chiko

- Species: harpy
- Age: 16
- Physique: skinny
- Hair: short, blond, unkempt
- Skin: light, freckled
- Wardrobe: tube top and short shorts
- Job: sky courier
- Personality: wild, manic

Lushu

- Species: ogre
- Age: 29
- Physique: muscular
- Hair: short, dark, messy
- Skin: tanned
- Wardrobe: chest wrap and fundoshi
- Job: builder, lifter, guard
- Personality: fierce, rough-and-tumble, protective

Jane

- Species: mermaid
- Age: 30
- Physique: athletic
- Hair: dark blue, short, ragged, always wet
- Eyes: orange
- Skin: light blue, gills under breasts
- Tail: blue scaly fish tail
- Teeth: sharp like piranha
- Wardrobe: scales covering nipples and groin, ear and lip piercings made of bone
- Job: sea hunter, scavenger
- Personality: sly, mischievous, intimidating

