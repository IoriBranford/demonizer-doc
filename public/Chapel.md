# Chapel

## Purpose

Introduce enemies protecting and saving humans from capture. Challenge Amy to prioritize attacks and capture more aggressively, instead of always relying on capture powers and letting them come to her.

## Difficulty

Normal.

## Estimated Time

Stage: 1 minute
Mid-Boss: 30 seconds
Boss: 45 seconds

## Background

...

## Story

Amy chases the remaining peasants, defended by Kingdom patrols, through the woods to the nearest Order chapel. There Amy first encounters Order priests with protective powers; defeating and capturing protected humans requires more risky tactics. At the altar, Amy faces the high priest and his enhanced spiritual defenses. When the high priest is near the end of his power, he throws out a final massive attack and flees via a secret passage. Hearing girls screaming for help inside, Amy follows.

## Feel

Slow and thoughtful. Enemies more defensive than aggressive.

## Areas

1. Road
   * Orbs arrive to protect fleeing enemy squads.
2. Entrance
   * Priest guards the doors.
3. Yard/Atrium
   * Statues spray dumb patterns to support guards and acolytes.
   * Colonnade on flanks providing enemy cover.
4. Chapel
   * Guards and priests take cover behind pews and pillars.
   * Sniper posts held up by pillars.
   * (Unsolvable) maze on the floor representing path to salvation.
5. Altar
   * High priest

## New Enemy

| Type        | Qty  | Defensive Ability                                            | Simultaneous Protect/Attack |
| ----------- | ---- | ------------------------------------------------------------ | --------------------------- |
| Priest      | Some | Few defense orbs                                             | N                           |
| Deacon      | One  | More defense orbs plus barrier                               | Y                           |
| Defense Orb | Some | Orbits target enemy, blocks player bullets, rescues KO humans |                             |
| High Priest | One  | Most defense orbs, strong barrier, melee attack strips captured humans and powers from player team | Y                           |

