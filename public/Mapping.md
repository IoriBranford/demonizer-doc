# Mapping

## Units

Property values in the map and objecttypes files use the following units:

- Distance = px
- Time = sec (except animation frame time)
- Speed = px/sec
- Animation frame time = msec
- Angle and orientation = deg
  - E = 0, S = 90, W = 180, N = 270
- Angular velocity = deg/sec

## Start map

1. Open template map `game/templates/stage.tmx` in Tiled
2. Save map as `<levelname>.tmx` in `game` directory
3. Resize map
   1. Horizontal can be slightly wider than camera for sway effect
   2. Vertical based on desired stage speed and time
4. Move player and camera to bottom center of map

## Stage path

1. In `camera` layer draw a polygon line from the camera's initial position (top-left) to its final position.

## Tile terrain

1. Add external tilesets with terrain
2. Add tile layer `<tileset>_low` at the bottom of the layer stack
3. Fill `<tileset>_low` with base terrain tile
4. Draw terrain in `<tileset>_low` (grass, dirt, road, ...)
5. Add tile layer `<tileset>_mid` directly over `<tileset>_low`
6. Draw decorations on `<tileset>_mid` (furniture, vegetation, ...)

## Enemies and other characters

1. Make a copy of the sample enemy group layer
2. Name the new enemy layer
3. Add external tileset(s) containing character sprites to the map
4. Place characters as tile objects into the layer at initial positions
5. Position and resize the trigger depending on desired activation
   1. By camera contact: on the map, within bounds such that the camera can't sway off it
   2. By other trigger: off the map
6. Draw polygon lines of character movement paths

## Correct enemy sprite sorting

1. Add empty object layer
3. Name object layer something like `floor1objects`, `floor2objects`, etc.
2. Position object layer in stack according to desired floor height
4. Set all enemy triggers' property `activateobjectslayer` to object layer name

## Tiles before enemies

1. Add tile layer `<tileset>_high` directly over ground object layer
2. Draw overhead or front tiles in `<tileset>_high`

## Boss fight

1. Create boss enemy group
2. In boss trigger, set properties:

| Key         | Value |
| ----------- | ----- |
| cameraspeed | 0     |
| cleartowin  | ☑     |

## Midboss fight

1. Create midboss enemy group
2. Create a post-midboss trigger in the same layer
3. In post-midboss trigger, set property `cameraspeed` to the original camera speed
4. In midboss trigger, set properties:

| Key            | Value                   |
| -------------- | ----------------------- |
| cameraspeed    | 0                       |
| cleartriggerid | post-midboss trigger id |
