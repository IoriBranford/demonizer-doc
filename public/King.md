# Belius

Amy and Angelina's father, ruler of the human Kingdom and commander-in-chief in the war against the demon realm. He discovered Amy's affair, put Jacob to death, then disowned and exiled Amy. When Belius hears of Amy returning as a succubus attacking the kingdom, he orders Angelina to lead the defense against her.

## Skills

* Variety of energy projectile attacks with sword
* Temp invulnerability shield
* Summon and command guards
* Cancel Amy's bomb

## Personality

Belius is cold, quiet, blunt, and uncaring, a man of more threat and action than words and emotion. He relentlessly seeks wealth and power not out of personal greed, but to further his ambition of ensuring security and prosperity for the human race, whatever the personal cost. He is absolutely devoted to running the Kingdom and expects the same from all his subjects.

### Interests

Military, government, law, politics, finance, technology

## Appearance

Even in his later years Belius is still a terrifying wall of muscle. His gray-blond mane and beard frame a joyless, battle-hardened face: eyes narrow and piercing, nose and mouth permanently bent into a disdainful grimace. He wears little to no jewelry and his crown is simply a spiked gold halo. His heavy sword is always at his hip or otherwise within reach.

In the throne room at the end of Amy's journey Belius will be ready in full battle armor, low in decorative details but polished to a pearly white mirror shine.

### References

King Fahn (Lodoss War)

![King Fahn (Lodoss War)](img/record_of_lodoss_war_online_king_fahn_by_hes6789-d8yu2ze.png)

