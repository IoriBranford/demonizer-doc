# Highway

## Purpose

Long and fast-moving gauntlet. Enemy mobs attack until defeated one after the other, without fleeing. Amy has no choice but to defeat them all.

## Difficulty

Challenging.

## Estimated Time

Stage: 2 minutes

Boss: 90 seconds

## Background

...

## Story

Amy emerges from the magma-flooded underground lab and charges down the road straight towards the capital city.

## Feel

Fast-paced high-pressure thrill ride. Enemies very fast and aggressive.

## Areas

1. Cavalry
2. Vehicles
3. City gates
   - High priest II

## New Enemy

| Type            | Qty  | Fighting Style                                               |
| --------------- | ---- | ------------------------------------------------------------ |
| Mounted Archer  | Many | Skirmisher, constant orbit and fire                          |
| Mounted Knight  | Many | Assaulter, approach with sword or fast charge with lance, or one after the other |
| Armored Vehicle | Few  | Crew fire from turrets                                       |
| High Priest II  | One  |                                                              |

