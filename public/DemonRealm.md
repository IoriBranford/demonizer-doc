# Demon Realm

## Purpose

The beginning of the game proper. A few easy battles against low-level Kingdom troops, giving the player a chance to pick up the basics: movement, shooting, focusing, capturing, converting.

## Story

Today is the Kingdom army's monthly monstergirl hunt. Amy and her friends' party is interrupted by reports of Kingdom troops abducting and slaughtering monster girls. Amy (and two fellow succubi? On easy mode maybe) ambushes them to stop the massacre, and discovers the thrill of bringing humans to her side, using her succubus powers to pacify and monsterize them for the first time.

Eventually she encounters Angelina leading the incursion. In an awkward reunion, Angelina realizes it's too late to bring Amy back, but is reluctant to fight her own sister. She tries warning off Amy, but Amy will not allow her friends to come to harm. Likewise, Amy begs Angelina to call off the attack, but Angelina is duty-bound to carry out the King's order.

In the middle of their duel, Angelina loses her nerve and ceases fire, but promises to return with a greater force. Amy threatens to counterattack and monsterize the Kingdom if the raids will not stop. Angelina sympathizes, but warns Amy that the Kingdom will spare no quarter, then flees.

## Enemy

| Type             | Move                                     | Speed    | Life   | Qty  | Strafe   |
| ---------------- | ---------------------------------------- | -------- | ------ | ---- | -------- |
| Pikemen          | Fixed path, usually in formation with others | Moderate | Low    | Many | None     |
| Archers          | Cover to cover                           | Fast     | Low    | Some | Sidestep |
| Swordsmen        | Charge player                            | Fast     | Medium | Some | None     |
| Assault Catapult | Fixed path (driven by operator)          | Moderate | Medium | Some | None     |
| Angelina (boss)  | Cover to cover                           | Fast     | High   | One  | Sidestep |

| Type             | Pattern       | Rate       | Bullet Speed | Range | FOV         |
| ---------------- | ------------- | ---------- | ------------ | ----- | ----------- |
| Pikemen          | Single        | Regular    | Moderate     | Long  | Narrow      |
| Archers          | Single        | Infrequent | Fast         | Long  | Wide        |
| Swordsmen        | Single        | Rapid      | Fast         | Short | Wide        |
| Assault Catapult | Narrow spread | Regular    | Moderate     | Long  | Fixed angle |
| Angelina (boss)  | Narrow spread | Rapid      | Fast         | Long  | Wide        |

## Estimated Time

Stage: 1m or less
Boss: 30-60s

## Areas

Stage Opening: Amy arrives on scene. Monster girls are running for their lives.
Intro to Core Gameplay: Slow-firing pikemen march along fixed routes. Mow them down and grab them for power and points.
Intro to Archers, Enemy Cover, Conversion: Archers are less numerous than pikemen, but they and their arrows move faster. They move from cover to cover, firing fast arrows in between. Capture and convert the sole female archer.
Intro to Enemy Volley Fire, Focus Mode: Try focus mode to weave through catapult volleys and have your new partner auto-target the enemy.
Intro to Swordsmen: You meet swordsmen about to execute a monster girl after burning down her home. They spot you and engage you in close combat.
Combination Attacks: Various combinations of enemy classes attacking together. e.g. Swordsmen+Catapults, Archers+Catapults.
Boss: Angelina is a master archer with some improvements over the common archer: rapid spread shot, dive into cover, arrow rain after charging for a couple seconds.

## Difficulty

Easy. Ample time to dodge, capture, and rescue.

## Nice to Add

Enemies crush flowers and small bushes when walking or rolling on them.
Non-living cover objects (e.g. large rocks, but not tall trees) which may be destroyed with the bomb.
