# Game Design

>**Spoiler Warning**: This page describes content that is best experienced in-game. Players should read with caution especially near sections on late-game content.

## Intro

The outcast princess turned succubus must stop cruel Kingdom troops from slaughtering friendly monstergirls in this 90s-arcade-style vertical shooting and man-catching challenge.

* Blow up war machines and break the soldiers' will with hot demonic seduction blasts
* Collect men to nourish you with spiritual energy; transform women to grow your succubus fleet
* Charge up a massive spiritual/demonic explosion to flush out soldiers in hiding

## Character Bios

### Amy
Amy is the player character and King Belius's youngest daughter. On her 16th birthday she was caught in bed with Jacob, the gardener's son and her only childhood friend. After his execution, Amy went into exile and eventually found her way to the neighboring demon realm, where she joined the friendly monstergirls and became a succubus.

Her powers are incapacitating eye beams (primary weapon), capturing and holding men as spiritual energy sources, a demonic kiss for transforming women, and an explosive reaction (bomb) from mixing accumulated spiritual energy with her demonic energy. When she first repels a Kingdom attack with these powers, she gets the idea to counterattack and literally demonize her former people until the monstergirl massacre stops.

### Angelina
Angelina is the older princess, heir to the throne, and a champion archer. She cares for Amy and has always tried to be a good sister, but is assigned to lead the fight against Amy and the monstergirls. Torn between love and duty, she appears at a few points in Amy's journey to discourage her advance, and to reluctantly fight her when she refuses to back down. As Amy comes closer to her ultimate objective (the King), Angelina grows increasingly desperate and fights her ever harder.

### Belius
King Belius is Amy and Angelina's father and mastermind of the war against the demon realm. He was the one who discovered Amy's affair, put Jacob to death and secretly disowned and exiled Amy. When Belius hears of Amy returning as a succubus attacking the kingdom, he orders Angelina to lead the defense against her.

Should Amy manage to confront Belius, he won't be pleased to see her, but he has enough respect for her accomplishments to explain himself before the final battle. Over just two generations, the royal family have built up a poor warrior clan into a military and industrial powerhouse serving thousands of people. He detests not only monstergirls but their way of life, always indulging and relying on nature's benevolence with no ambition or endeavor of their own. He swears to strike her down before she drags his people back into the dirt.

## Rough Plot

### Prologue
Princess Amy has fled into the demon realm after her best friend and lover was put to death. Intoxicated by the realm's demonic energy, Amy collapses, and monster girls find her and close in on her. But she discovers their true nature as they care for and comfort her instead of viciously devouring her. One of them, a motherly succubus, gives Amy a kiss that transforms her into a new succubus. Thus Amy joins the monster girl family.

### Stage 1
Amy and her friends' party is interrupted by reports of Kingdom troops abducting and slaughtering monster girls. Amy (and two fellow succubi?) ambushes them to stop the massacre, and discovers the thrill of bringing humans to her side, using her succubus powers to pacify and monsterize them for the first time. Eventually she encounters Angelina leading the incursion.

In an awkward reunion, Angelina realizes it's too late to bring Amy back, but is reluctant to fight her own sister. She tries warning off Amy, but Amy will not allow her friends to come to harm. Likewise, Amy begs Angelina to call off the attack, but Angelina is duty-bound to carry out the King's order. In the middle of their duel, Angelina loses her nerve and ceases fire, but promises to return with a greater force. Amy threatens to counterattack and monsterize the Kingdom if their attacks will not stop. Angelina sympathizes, but warns Amy that the Kingdom will spare no quarter, then flees.

### Stage 2
Amy attacks a small farming village on the border of the Kingdom. The peasants fight back with support from Kingdom troops. Eventually the lord and his guard arrive and engage Amy, but they are no match. Encouraged by their first victory, Amy and her friends press on.

### Stage 3
Two possibilities for this stage.

#### Chapel
Amy chases the remaining peasants, defended by Kingdom patrols, through the woods to the nearest Order chapel. There Amy first encounters Order priests with protective powers; defeating and capturing protected humans requires more risky tactics. At the altar, Amy faces the high priest and his enhanced spiritual defenses. When the high priest is near the end of his power, he throws out a final massive attack and flees via a secret passage. Hearing girls screaming for help inside, Amy follows.

#### River
Amy travels upriver attacking fishing villages. Squads of militia attack from boats.

### Stage 4
Amy discovers an underground lab where research teams subject captive monster girls to experiments and interrogation. Some monster girls are confined in cages or chains; Amy should destroy these on top of defeating the torturers. Other monster girls are mind-controlled into fighting Amy and will self-destruct if defeated, so Amy should defeat the controller instead. When Amy rescues monster girls, they either flee, or fight alongside her. Late in the stage Amy meets the mage in charge of research, who can put freed monster girls back under confinement or control, in which case Amy should free them again in the same way as before.

### Stage 5
Amy runs the gauntlet up the path to the Kingdom's capital, taking on various mounted units and armored vehicles trying to intercept her. When Amy defeats the largest armored vehicle, its brakes are disabled so it continues rolling towards the city. But the high priest from Stage 3, with help from some Order brothers, has locked down the capital with a protective barrier. The armored vehicle smashes against the city gates, but the barrier keeps the gates intact. To proceed, Amy must defeat all of the priests holding up the barrier. On defeating the high priest Amy has only a short time to capture him before he takes his own life.

### Stage 6
Amy enters the capital city. The city guard puts up heavy resistance with advanced weapons, hardened defenses and rapid ambushes.

[The city guard station]

[The castle gates]

### Stage 7
Inside the castle Amy must contend with the Royal Guard, the Kingdom's most elite fighting force. Guardsmen can not only block with shields, but parry her attacks with their own. The Order archbishop can not only shield his comrades, but revive defeated men given enough time. Archmages have perfected the monster-controlling techniques from Stage 4 and can turn Amy's friends against her; again the player should save them by defeating the controller. Women of the Guard are prepared to commit suicide or be euthanized to prevent their monsterization if defeated.

At last Amy reaches the throne room where King Belius and Angelina are waiting. In desperation Angelina offers to join Amy if Amy will stand down, but Belius traps Angelina in a protective barrier. As much as it pains Belius to see his child monsterized and his kingdom falling, he grudgingly commends her courage and leadership. Nevertheless, a long and brutal battle ensues in which Belius barrages Amy with massive attacks along with all of the Royal Guard's special techniques.

When Amy finally depletes Belius's power, he lets himself into the barrier with Angelina, where he whispers a prayer and runs his sword through Angelina and himself. Though Belius dies, Angelina gets back on her feet, unaffected by what should be a fatal wound, and realizes what her father has really done. Angelina cries out in protest, but a ray of light from the Chief God Himself falls on her, transforming her into an angel. Possessed with holy rage, Angelina flies shrieking through the skylight above. Amy follows.

### Final Stage
The vengeful angel Angelina rains destruction upon human and demonic lands alike, intent on wiping the demon realm off the face of the world. Amy chases her and a spectacular final battle ensues. At last Amy prevails, Angelina's shredded wings fail and she starts to plummet back to earth. Amy swoops down after Angelina and saves her. Still enraged, Angelina resists with all her remaining strength, but not enough to stop Amy from joining lips with her. The resulting flash of alternating light and darkness can be seen for miles.

### Bonus Stage
Back in the ruined city, the remnants of the Kingdom army come across Amy and Angelina facing each other. They prepare to engage Amy as she snarls uncharacteristic threats at Angelina, but Angelina orders them to stand down. They obey, trusting their new angel queen to win the day. Amy and Angelina duel a final time. It ends with Angelina scoring a winning but not lethal hit. Amy and her fellow succubi retreat, but promise to return. Angelina and the remaining humans celebrate victory.

### Epilogue
The truth is revealed late that night after the celebrations end. Angelina is preparing her bath when Amy joins her, still sore from the training arrow that hit her. The bathwater washes the whitener out of Angelina's wings, revealing them to be black. Amy wants to spend the night with Angelina, catching up with each other and being together again, but Angelina has to lead the vigil for their late father.

The next morning, while Angelina and her remaining people lay Belius to rest, Amy visits the garden where she met Jacob. She picks his favorite flowers, then leaves them at his grave before returning to the monster realm.

## Gameplay Description

* You are in a playfield that moves along a fixed path through the stage.
* When the playfield reaches an enemy encounter trigger, the associated enemies enter the playfield.
  * Move within the playfield to dodge enemy fire and shoot the enemy down with magic attacks.
* Touch defeated human enemies to capture them.
  * Capture men to score points, power up your team, and gather energy for your bomb attack.
  * Capture women to turn them into succubi who join you. They either fight alongside you as wingmen, or wait in reserve to do so.
* Focus fire mode is geared towards weaving between bullets. This mode lets you focus on dodging as wingmen auto-aim at enemies, and powered-up wingmen auto-capture nearby humans.
  * When playing with a controller or keyboard, your movement within the playfield is slowed to facilitate precision movement.
* You carry a limited supply of bombs. Drop a bomb to cause an explosion dealing massive damage and sending defeated humans to you.
  * If you have reserve wingmen, some of them will enter the fray while the bomb is active, finding the closest enemy to either shoot point-blank or capture.
* You win the stage when you defeat the powerful enemy at the end.
* You are destroyed if the enemy lands a single hit on you.
  * If you have a spare life, you can respawn and your wingmen will rejoin you.

## Artistic Style Outline

### Visual
Overall fantastic and colorful. Diverse and saturated palette; vivid cartoon shading with a minimum of shadows and surface details.

Game graphics are early-to-mid-90s arcade and console style pixel art. In-game characters may be slightly super-deformed for better visibility. See:

* [Final Fantasy 6](http://www.mobygames.com/game/snes/final-fantasy-iii__/screenshots)
* [Undeadline](http://www.mobygames.com/game/undead-line/screenshots)
* [Mahou Daisakusen](http://www.mobygames.com/game/arcade/sorcer-striker/screenshots)

Detailed character and environment views are idealized, but shapes and proportions are realistic or semi-realistic. See:

* [Hiroya Oku](http://www.zerochan.net/Oku+Hiroya)
* [Akihiro Yamada](http://www.zerochan.net/Yamada+Akihiro)
* [Takeshi Obata](http://www.zerochan.net/Obata+Takeshi)

### Audio
Upbeat, exciting and impactful. Prefer FM synth instruments and noises, again similar to early-to-mid-90s arcade and console games.

Sharp and hard-hitting SFX. See:

* [Shinobi III](https://youtu.be/uNGbAStPXZc)
* [Thunder Force IV](https://youtu.be/DqgjqBDYufM)
* [Chorensha 68k](https://youtu.be/7oXQcSN-8F8)

Fun 90s rock-and-roll soundtrack; refer to [Battle Mania Daiginjou](http://project2612.org/details.php?id=7) and [Joan Jett](https://www.youtube.com/watch?v=8fQFSBJlhLc&list=PLBfzSMzWj2voElsxJxEKgx6r_NHe-h7Xn&index=1).

Mood keywords:

- Stage 1: urgent, crisis, defense, take a stand
- Stage 2: let's go, attack, have fun, badass
- Stage 3: think, focus, be careful, take your time
- Stage 4: dark, dangerous, cruel, nasty, save them
- Stage 5: fly high, gotta go fast, hurry up
- Stage 6: heating up, entering enemy territory
- Stage 7: final, this is it, almost there
- Final Stage/Boss: doom, catastrophe, finish it, save the world
- Boss 1/7:
- Boss 2: heavy, mighty, armored warrior
- Boss 3: 
- Boss 4: master of fire
- Boss 5: 
- Boss 6: 
- Bonus Stage: party, 

## Systematic Breakdown of Components

* LÖVE 2D game framework
	* 2D rendering functions
		* Draw sprites and particle systems from images
		* Draw text from fonts
		* Draw shapes
		* Render to scaled texture to preserve pixel art integrity
	* Physics engine
		* Management of rigid bodies, colliders, contacts
		* Selective collision detection flags
	* Event system for input events, OS events, hardware events
	* File system for saving user's data
		* Options
		* Progress
		* Records
* Tiled map editor + Simple Tiled Implementation for LÖVE + Levity module
	* Multi-layered 2D maps
		* Tile and object layers
		* Tileset and sprite images
* Gameplay
	* Automoving camera/playfield
	* Enemy encounter start/end triggers
		* Space trigger - during camera collision
		* Time trigger - when a timer hits fixed start time and/or end time
		* Sequential trigger - start after the end of a preceding enemy encounter
		* Manual trigger - called in script
	* Player team
		* Player captures women to add them to team
		* Teammates fly and fire in formation with player
		* Reserves replace lost teammates
	* Scoring system
		* Points for defeating and capturing enemy
		* Capturing men increases capture points multiplier

## Suggested Game Flow

* Title - player choices:
	* Start new game
	* Start at any previously-reached level
	* Start challenge stage
	* Options
	* Records
* Prologue
* Tutorial
	* Move
	* Shoot
	* Focus
	* Capture
	* Powerup
	* Bomb
* Stages 1-6:
	* Enter stage
	* Stage
		* One or more mid-bosses possible
	* Boss intro
	* Boss
	* If player destroyed before stage clear, game over
	* Stage clear
* Stage 7 - as a normal stage, but instead of stage clear:
	* Boss self-destruction
	* Final boss intro
	* Continue to final stage
* Final Stage
	* Final boss
	* If final boss destroyed, Bonus Stage
* Game Over if player destroyed - player choices:
	* Restart current stage
	* End game, go to game results
* Bonus Stage - fake battle with Angelina
    * Survive as long as possible
    * Angelina is invulnerable and grows more difficult over time
    * When player hit:
	    * Bonus for time survived and shots landed on Angelina
	    * Bonus for extra lives remaining
	    * Continue to Epilogue
* Epilogue
* Game Results
	* When done, return to Title

## Additional Ideas and Possibilities

* Extra stages
    * "Caravan" time attack
    * Exercises
    * Minigames
* Gallery
    * Collectible cards of captured humans

